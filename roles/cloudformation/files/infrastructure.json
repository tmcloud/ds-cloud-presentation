{
  "AWSTemplateFormatVersion" : "2010-09-09",

  "Description" : "AWS CF: EC2+SG+Route53_Record",

  "Parameters" : {
    "KeyName": {
      "Description" : "Name of an existing EC2 KeyPair to enable SSH access to the instance",
      "Type": "AWS::EC2::KeyPair::KeyName",
      "Default" : "secdash",
      "ConstraintDescription" : "must be the name of an existing EC2 KeyPair."
    },

    "InstanceType" : {
      "Description" : "WebServer EC2 instance type",
      "Type" : "String",
      "Default" : "t2.nano",
      "AllowedValues" : [ "t1.micro", "t2.nano", "t2.micro", "t2.small"]
,
      "ConstraintDescription" : "must be a valid EC2 instance type."
    },

    "HostedZone" : {
      "Description" : "The DNS name of an existing Amazon Route 53 hosted zone",
      "Type" : "String"
     },

    "VPC": {
        "Description" : "Id of my VPC",
        "Type"        : "String"
    },

    "ProjectName": {
        "Description" : "CI Projec Name",
        "Type"        : "String"
    },

    "BuildVersion": {
        "Description" : "CI build version",
        "Type"        : "String"
    },

    "Subnet": {
        "Description" : "VPC Public Subnet",
        "Type": "String"
    },

    "WEBLocation" : {
      "Description" : "The IP address range that can be used to SSH to the EC2 instances",
      "Type": "String",
      "MinLength": "9",
      "MaxLength": "18",
      "Default": "0.0.0.0/0",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "ConstraintDescription": "must be a valid IP CIDR range of the form x.x.x.x/x."
   },

    "SSHLocation" : {
      "Description" : "The IP address range that can be used to SSH to the EC2 instances",
      "Type": "String",
      "MinLength": "9",
      "MaxLength": "18",
      "Default": "0.0.0.0/0",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "ConstraintDescription": "must be a valid IP CIDR range of the form x.x.x.x/x."
   },

   "VPCLocation" : {
     "Description" : "The IP address range that can be used to SSH to the EC2 instances",
     "Type": "String",
     "MinLength": "9",
     "MaxLength": "18",
     "Default": "172.0.0.0/8",
     "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
     "ConstraintDescription": "must be a valid IP CIDR range of the form x.x.x.x/x."
  },

  "ZabbixMetadata" : {
    "Description" : "Zabbix Metadata",
    "Default": "default",
    "Type": "String",
    "MinLength": "1",
    "MaxLength": "32"
 },

 "AppVersion" : {
   "Description" : "Application Version",
   "Default": "nover",
   "Type": "String",
   "MinLength": "1",
   "MaxLength": "10"
},

 "IAMRole" : {
   "Description" : "IAM Role",
   "Type": "String",
   "MinLength": "1",
   "MaxLength": "32"
},

   "DockerRepo" : {
     "NoEcho": "true",
     "Description" : "The Docker Cloud Registry account username",
     "Type": "String",
     "MinLength": "1",
     "MaxLength": "16"
 },

    "DockerUser" : {
      "NoEcho": "true",
      "Description" : "The Docker Cloud Registry account username",
      "Type": "String",
      "MinLength": "1",
      "MaxLength": "16"
  },

    "DockerPassword" : {
      "NoEcho": "true",
      "Description" : "The Docker Cloud Registry account password",
      "Type": "String",
      "MinLength": "1",
      "MaxLength": "20"
  },

    "DockerEmail" : {
      "NoEcho": "true",
      "Description" : "The Docker Cloud Registry account email",
      "Type": "String",
      "MinLength": "1",
      "MaxLength": "30"
    }
  },

  "Mappings" : {
    "AWSInstanceType2Arch" : {
      "t2.nano"     : { "Arch" : "HVM64"  },
      "t2.micro"    : { "Arch" : "HVM64"  },
      "t2.small"    : { "Arch" : "HVM64"  }
    },

    "AWSRegionArch2AMI" : {
      "us-east-1"        : { "HVM64" : "ami-08c4c11f" }
    }

},

  "Resources" : {
    "EC2Instance" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "InstanceType" : { "Ref" : "InstanceType" },
        "SecurityGroupIds" : [ { "Ref" : "InstanceSecurityGroup" } ],
        "KeyName" : { "Ref" : "KeyName" },
        "SubnetId": { "Ref": "Subnet" },
        "IamInstanceProfile" : { "Ref": "IAMRole" },
        "ImageId" : { "Fn::FindInMap" : [ "AWSRegionArch2AMI", { "Ref" : "AWS::Region" },
                          { "Fn::FindInMap" : [ "AWSInstanceType2Arch", { "Ref" : "InstanceType" }, "Arch" ] } ] },
        "Tags" : [ {
           "Key" : "Name",
           "Value" : { "Ref" : "ZabbixMetadata" },
            },
            {
              "Key" : "AppVersion",
              "Value" : { "Ref" : "AppVersion" },
            },
            {
                "Key" : "Project",
                "Value" : "cloud-ari-bottino"
            },
            {
                "Key" : "zabbix-metadata",
                "Value" : { "Ref" : "ZabbixMetadata"
            }
              }],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : ["", [
               "#!/bin/bash -xe\n",
               "/opt/infra/ch-hostname.sh ", { "Ref" : "ZabbixMetadata" },"\n",
               "touch /etc/use_dsa_with_iptables\n",
               "yum install docker -y\n",
               "chkconfig docker on\n",
               "service docker start\n",
               "docker login",
               " -e ", { "Ref" : "DockerEmail" },
               " -u ", { "Ref" : "DockerUser" },
               " -p ", { "Ref" : "DockerPassword" }, "\n",
               "docker pull ", { "Ref" : "DockerRepo" },"/", { "Ref" : "ProjectName" },"\n",
               "wget https://app.deepsecurity.trendmicro.com:443/software/agent/amzn1/x86_64/ -O /tmp/agent.rpm --quiet\n",
               "rpm -ihv /tmp/agent.rpm\n",
               "sleep 15\n",
               "/opt/ds_agent/dsa_control -r\n",
               "/opt/ds_agent/dsa_control -a dsm://agents.deepsecurity.trendmicro.com:443/ 'tenantID:48099067-72D3-5BA3-D24C-4817FB2DE13C' 'tenantPassword:83618121-10B3-A62A-ECF8-81E9E8070423' 'policyid:522'\n",
               "docker run --name=", { "Ref" : "ProjectName" }, " --restart=always -h ", { "Ref" : "ProjectName" }, " -p 80:8000 --log-driver=gelf --log-opt gelf-address=udp://syslog.tmcloud.com.br:12201 --log-opt tag=", { "Ref" : "ProjectName" }, " -d ", { "Ref" : "DockerRepo" }, "/", { "Ref" : "ProjectName" },"\n"
               ]]}}
      }
    },

   "DNSRecord" : {
      "Type" : "AWS::Route53::RecordSet",
      "Properties" : {
          "HostedZoneName" : {
            "Fn::Join" : [ "", [
               { "Ref" : "HostedZone" }, "."
            ] ]
         },
         "Comment" : "DNS name for my instance.",
         "Name" : {
            "Fn::Join" : [ "", [
               {"Ref" : "ProjectName"}, "-",
               {"Ref" : "BuildVersion"}, ".",
               {"Ref" : "HostedZone"} ,"."
            ] ]
         },
         "Type" : "A",
         "TTL" : "900",
         "ResourceRecords" : [
            { "Fn::GetAtt" : [ "EC2Instance", "PublicIp" ] }
         ]
      }
   },

    "InstanceSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "VpcId" : { "Ref" : "VPC" },
        "GroupDescription" : "Enable SSH, HTTP and Monitoring",
        "SecurityGroupIngress" : [ {
          "IpProtocol" : "tcp",
          "FromPort" : "22",
          "ToPort" : "22",
          "CidrIp" : { "Ref" : "SSHLocation"}
        },
        {
          "IpProtocol" : "tcp",
          "FromPort" : "10050",
          "ToPort" : "10050",
          "CidrIp" : { "Ref" : "VPCLocation"}
        },
        {
          "IpProtocol" : "tcp",
          "FromPort" : "80",
          "ToPort" : "80",
          "CidrIp" : { "Ref" : "WEBLocation"}
        } ]
      }
    }
},

  "Outputs" : {
    "InstanceId" : {
      "Description" : "InstanceId of the newly created EC2 instance",
      "Value" : { "Ref" : "EC2Instance" }
    },
    "AZ" : {
      "Description" : "Availability Zone of the newly created EC2 instance",
      "Value" : { "Fn::GetAtt" : [ "EC2Instance", "AvailabilityZone" ] }
    },
    "PublicDNS" : {
      "Description" : "Public DNSName of the newly created EC2 instance",
      "Value" : { "Fn::GetAtt" : [ "EC2Instance", "PublicDnsName" ] }
    },
    "PublicIP" : {
      "Description" : "Public IP address of the newly created EC2 instance",
      "Value" : { "Fn::GetAtt" : [ "EC2Instance", "PublicIp" ] }
    }
  }
}
