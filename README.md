# **TrendMicro Deep Security - Apresentação (ds-cloud)** #

# **Table of Contents:** #

## Cloud / Container / Docker / DevOps ##

# Agenda #
* Trend Micro
* Shared Responsibility
    * Monitoração
    * Controle de vulnerabilidade
    * Persistência dos eventos
    * Automação
    * Availability Strategy
* Importância de uma plataforma
* O que é o Deep Security?
* Como o Deep Security pode ajudar a jornada para a nuvem
    * Encaminhando eventos para monitoração
    * Data Driven: SIEM
    * Automação: Chef/Ansible/Puppet
    * Hybrid Cloud
    * as a Service
